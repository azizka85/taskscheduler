﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace TaskScheduler.Extensions
{
    public static class AsyncExtensions
    {
        public static CancellationTokenAwaiter GetAwaiter(this CancellationToken ct)
        {
            return new CancellationTokenAwaiter
            {
                CancellationToken = ct,
            };
        }

        public struct CancellationTokenAwaiter : ICriticalNotifyCompletion
        {
            public CancellationTokenAwaiter(CancellationToken cancellationToken)
            {
                CancellationToken = cancellationToken;
            }

            internal CancellationToken CancellationToken;

            public object GetResult()
            {
                return IsCompleted;
            }

            public bool IsCompleted => CancellationToken.IsCancellationRequested;

            public void OnCompleted(Action continuation) => CancellationToken.Register(continuation);

            public void UnsafeOnCompleted(Action continuation) => CancellationToken.Register(continuation);
        }
    }
}